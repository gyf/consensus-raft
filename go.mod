module chainmaker.org/chainmaker/consensus-raft/v2

go 1.15

require (
	chainmaker.org/chainmaker/chainconf/v2 v2.2.0
	chainmaker.org/chainmaker/common/v2 v2.2.0
	chainmaker.org/chainmaker/consensus-utils/v2 v2.2.0
	chainmaker.org/chainmaker/localconf/v2 v2.2.0
	chainmaker.org/chainmaker/logger/v2 v2.2.0
	chainmaker.org/chainmaker/pb-go/v2 v2.2.0
	chainmaker.org/chainmaker/protocol/v2 v2.2.0
	chainmaker.org/chainmaker/raftwal/v2 v2.1.0
	chainmaker.org/chainmaker/utils/v2 v2.2.0
	github.com/gogo/protobuf v1.3.2
	github.com/golang/mock v1.6.0
	github.com/stretchr/testify v1.7.0
	github.com/thoas/go-funk v0.9.1
	go.etcd.io/etcd/client/pkg/v3 v3.5.1
	go.etcd.io/etcd/raft/v3 v3.5.1
	go.etcd.io/etcd/server/v3 v3.5.1
	go.uber.org/zap v1.19.1
)
