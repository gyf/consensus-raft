/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package raft

import (
	"chainmaker.org/chainmaker/logger/v2"
	"github.com/golang/mock/gomock"
	"os"
	"os/exec"
	"testing"
	"time"

	consensus_utils "chainmaker.org/chainmaker/consensus-utils/v2"
	"chainmaker.org/chainmaker/consensus-utils/v2/testframework"
	consensuspb "chainmaker.org/chainmaker/pb-go/v2/consensus"
	"chainmaker.org/chainmaker/protocol/v2"
	"github.com/stretchr/testify/require"
)

var (
	nodeNum          = 3
	chainId          = "chain1"
	consensusType    = consensuspb.ConsensusType_RAFT
	ConsensusEngines = make([]protocol.ConsensusEngine, nodeNum)
	CoreEngines      = make([]protocol.CoreEngine, nodeNum)
)

func TestOnlyConsensus_RAFT(t *testing.T) {
	cmd := exec.Command("/bin/sh", "-c", "rm -rf chain1 default.*")
	err := cmd.Run()
	require.Nil(t, err)

	err = os.Mkdir("chain1", 0750)
	require.Nil(t, err)
	defer os.RemoveAll("chain1")

	err = testframework.InitLocalConfigs()
	require.Nil(t, err)
	defer testframework.RemoveLocalConfigs()

	testframework.SetTxSizeAndTxNum(300, 10000)

	// init chainConfig and LocalConfig
	testframework.InitChainConfig(chainId, consensusType, nodeNum)
	testframework.InitLocalConfig(nodeNum)

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	testNodeConfig, err := testframework.CreateTestNodeConfig(ctrl, nodeNum, chainId, consensusType, nil)
	require.Nil(t, err)

	cmLogger := logger.GetLogger(chainId)
	for i := 0; i < nodeNum; i++ {
		// new CoreEngine
		CoreEngines[i] = testframework.NewCoreEngineForTest(testNodeConfig[i], cmLogger)
	}

	var consensus *ConsensusRaftImpl
	for i := 0; i < nodeNum; i++ {
		rc := &consensus_utils.ConsensusImplConfig{
			ChainId:     testNodeConfig[i].ChainID,
			NodeId:      testNodeConfig[i].NodeId,
			Ac:          testNodeConfig[i].Ac,
			Core:        CoreEngines[i],
			ChainConf:   testNodeConfig[i].ChainConf,
			Signer:      testNodeConfig[i].Signer,
			LedgerCache: testNodeConfig[i].LedgerCache,
			MsgBus:      testNodeConfig[i].MsgBus,
		}

		consensus, err = New(rc)
		consensus.electionTick = 2
		if err != nil {
			require.Nil(t, err)
		}
		ConsensusEngines[i] = consensus
	}

	tf, err := testframework.NewTestClusterFramework(chainId, consensusType, nodeNum, testNodeConfig, ConsensusEngines, CoreEngines)
	require.Nil(t, err)

	tf.Start()
	time.Sleep(10 * time.Second)
	tf.Stop()

}
